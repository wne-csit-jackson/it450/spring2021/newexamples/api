# swagger-cli

This is a 3rd-party Docker container for validating and bundling OpenAPI
specifications from the command line.

## 1. Users

### 1.1. Build

```
bin/dev/build
```

### 1.2. Use

```
bin/usr/swagger-cli
```

## 2. Developers

### 2.1. Manage Dependencies

```
bin/dev/yarn
```

# Items API

This is an OpenAPI specification of the Items API.

## 1. OpenAPI Specification

The entry point of the specification is in `src/index.yaml`

## 2. Tools

### 2.1. Build tools

```
bin/dev/build
```

### 2.2. Validate specification

```
bin/dev/validate
```

### 2.3. Bundle specification into a single file

```
bin/dev/bundle --outfile items-api.yaml
```
